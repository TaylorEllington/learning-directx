#pragma once

#include "d3d-class.h"
#include "camera-class.h"
#include "model-class.h"
#include "light-shader-class.h"
#include "light-class.h"



const bool FULL_SCREEN = false;
const bool VSYNC_ENABLED = true;
const float SCREEN_DEPTH = 1000.0f;
const float SCREEN_NEAR = 0.1f;

class GraphicsClass
{
public:
    GraphicsClass();
    GraphicsClass(const GraphicsClass &);
    ~GraphicsClass();

    bool Initialize(int, int, HWND);
    void Shutdown();
    bool Frame();

private:
    bool Render(float);

    D3DClass * mD3D;
    CameraClass * mCamera;
    ModelClass * mModel;
    LightShaderClass* mLightShader;
    LightClass* mLight;

};