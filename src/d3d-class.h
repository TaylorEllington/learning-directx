#pragma once

#include <d3d11.h>
#include <DirectXMath.h>

class D3DClass {

public:
    D3DClass();
    D3DClass(const D3DClass&);
    ~D3DClass();

    bool Initialize(int, int, bool, HWND, bool, float, float);
    void Shutdown();

    void BeginScene(float, float, float, float);
    void EndScene();

    ID3D11Device* GetDevice();
    ID3D11DeviceContext* GetDeviceContext();

    void GetProjectionMatrix(DirectX::XMMATRIX&);
    void GetWorldMatrix(DirectX::XMMATRIX&);
    void GetOrthoMatrix(DirectX::XMMATRIX&);

    void GetVideoCardInfo(char*, int&);

private:

    bool mVsyncEnabled;
    int mVideoCardMemory;
    char mVideoCardDescription[128];
    IDXGISwapChain* mSwapChain;
    ID3D11Device* mDevice;
    ID3D11DeviceContext* mDeviceContext;
    ID3D11RenderTargetView* mRenderTargetView;
    ID3D11Texture2D* mDepthStencilBuffer;
    ID3D11DepthStencilState* mDepthStencilState;
    ID3D11DepthStencilView* mDepthStencilView;
    ID3D11RasterizerState* mRasterState;
    DirectX::XMMATRIX mProjectionMatrix;
    DirectX::XMMATRIX mWorldMatrix;
    DirectX::XMMATRIX mOrthoMatrix;

};