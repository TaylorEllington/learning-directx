#include "graphics-class.h"


GraphicsClass::GraphicsClass()
{
    mD3D = nullptr;
    mCamera = nullptr;
    mModel = nullptr;
    mLightShader = nullptr;
    mLight = nullptr;
}


GraphicsClass::GraphicsClass(const GraphicsClass& other)
{
}


GraphicsClass::~GraphicsClass()
{
}


bool GraphicsClass::Initialize(int screenWidth, int screenHeight, HWND hwnd)
{
    bool result;

    mD3D = new D3DClass;
    if(!mD3D){
        return false;
    }

    result  = mD3D->Initialize( screenWidth,
                                screenHeight,
                                VSYNC_ENABLED,
                                hwnd,
                                FULL_SCREEN,
                                SCREEN_DEPTH,
                                SCREEN_NEAR);

    if(!result){
        MessageBox(hwnd, "Could not init Direct3D", "Error", MB_OK);
        return false;
    }

    // Create the camera object.
    mCamera = new CameraClass;
    if(!mCamera)
    {
        return false;
    }

    // Set the initial position of the camera.
    mCamera->SetPosition(0.0f, 0.0f, -10.0f);

    
    // Create the model object.
    mModel = new ModelClass;
    if(!mModel)
    {
        return false;
    }

    // Initialize the model object.
    result = mModel->Initialize(mD3D->GetDevice(), "resources/cube.txt" , L"resources/seafloor.dds");
    if(!result)
    {
        MessageBoxW(hwnd, L"Could not initialize the model object.", L"Error", MB_OK);
        return false;
    }

    // Create the light shader object.
    mLightShader = new LightShaderClass;
    if(!mLightShader)
    {
        return false;
    }

    // Initialize the light shader object.
    result = mLightShader->Initialize(mD3D->GetDevice(), hwnd);
    if(!result)
    {
        MessageBoxW(hwnd, L"Could not initialize the light shader object.", L"Error", MB_OK);
        return false;
    }

    // Create the light object.
    mLight = new LightClass;
    if(!mLight)
    {
        return false;
    }

    // Initialize the light object.
    mLight->SetDiffuseColor(1.0f, 1.0f, 1.0f, 1.0f);
    mLight->SetDirection(0.0f, 0.0f, 1.0f);

    

    return true;
}


void GraphicsClass::Shutdown()
{
    // Release the light object.
    if(mLight)
    {
        delete mLight;
        mLight = nullptr;
    }

    // Release the light shader object.
    if(mLightShader)
    {
        mLightShader->Shutdown();
        delete mLightShader;
        mLightShader = nullptr;
    }


    // Release the model object.
    if(mModel)
    {
        mModel->Shutdown();
        delete mModel;
        mModel = nullptr;
    }

    // Release the camera object.
    if(mCamera)
    {
        delete mCamera;
        mCamera = nullptr;
    }

    if(mD3D){
        mD3D->Shutdown();
        delete mD3D;
        mD3D = nullptr;
    }
    return;
}


bool GraphicsClass::Frame()
{
    bool result;

    //eww static? here?
    static float rotation = 0.0f;


    // Update the rotation variable each frame.
    rotation += (float)DirectX::XM_PI * 0.01f;
    if(rotation > 360.0f)
    {
        rotation -= 360.0f;
    }


    result = Render(rotation);
    if(!result){
        return false;
    }
    return true;
}


bool GraphicsClass::Render(float rotation)
{

    DirectX::XMMATRIX viewMatrix, projectionMatrix, worldMatrix;
    bool result;


    // Clear the buffers to begin the scene.
    mD3D->BeginScene(0.0f, 0.0f, 0.0f,1.0f);

    // Generate the view matrix based on the camera's position.
    mCamera->Render();

    // Get the world, view, and projection matrices from the camera and d3d objects.
    mCamera->GetViewMatrix(viewMatrix);
    mD3D->GetWorldMatrix(worldMatrix);
    mD3D->GetProjectionMatrix(projectionMatrix);

    // Rotate the world matrix by the rotation value so that the triangle will spin.
    worldMatrix = DirectX::XMMatrixRotationY(rotation);

    // Put the model vertex and index buffers on the graphics pipeline to prepare them for drawing.
    mModel->Render(mD3D->GetDeviceContext());


    // Render the model using the light shader.
    result = mLightShader->Render(mD3D->GetDeviceContext(), mModel->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix, 
                       mModel->GetTexture(), mLight->GetDirection(), mLight->GetDiffuseColor());
    if(!result)
    {
        return false;
    }

    mD3D->EndScene();

    return true;
}