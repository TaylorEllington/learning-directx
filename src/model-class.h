#pragma once

#include <d3d11.h>
#include <DirectXMath.h>
#include <fstream>

#include "texture-class.h"


class ModelClass {
private:
    struct VertexType
    {
        DirectX::XMFLOAT3 position;
        DirectX::XMFLOAT2 texture;
        DirectX::XMFLOAT3 normal;

    };

    struct ModelType
    {
        float x, y, z;
        float tu, tv;
        float nx, ny, nz;
    };

public:
    ModelClass();
    ModelClass(const ModelClass&);
    ~ModelClass();

    bool Initialize(ID3D11Device*, char*,  WCHAR*);
    void Shutdown();
    void Render(ID3D11DeviceContext*);

    int GetIndexCount();

    ID3D11ShaderResourceView* GetTexture();
    bool LoadTexture(ID3D11Device*, WCHAR*);
    void ReleaseTexture();

    bool LoadModel(char *);
    void ReleaseModel();

private:
    bool InitializeBuffers(ID3D11Device*);
    void ShutdownBuffers();
    void RenderBuffers(ID3D11DeviceContext*);

    ID3D11Buffer * mVertexBuffer, * mIndexBuffer;
    int mVertexCount, mIndexCount;

    TextureClass * mTexture;
    ModelType * mModel;

};