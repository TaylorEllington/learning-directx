#include "texture-class.h"
#include "DDSTextureLoader.h"

TextureClass::TextureClass()
{
    mTexture = nullptr;
    mTextureView = nullptr;
}


TextureClass::TextureClass(const TextureClass& other)
{
}


TextureClass::~TextureClass()
{
}

bool TextureClass::Initialize(ID3D11Device* device, WCHAR* filename)
{
    HRESULT result;
    size_t size;

    // Load the texture in.
    result = DirectX::CreateDDSTextureFromFile(device, filename, &mTexture, &mTextureView, 0, nullptr);

    if(FAILED(result))
    {
        return false;
    }

    return true;
}

void TextureClass::Shutdown()
{
    // Release the texture resource.
    if (mTextureView)
    {
        mTextureView->Release();
        mTextureView = nullptr;
    }


    if(mTexture)
    {
        mTexture->Release();
        mTexture = nullptr;
    }

    return;
}

ID3D11ShaderResourceView* TextureClass::GetTextureView()
{
    return mTextureView;
}

ID3D11Resource* TextureClass::getTexture()
{
    return mTexture;
}
