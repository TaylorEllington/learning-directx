#include "light-class.h"


LightClass::LightClass()
{
}


LightClass::LightClass(const LightClass& other)
{
}


LightClass::~LightClass()
{
}


void LightClass::SetDiffuseColor(float red, float green, float blue, float alpha)
{
    mDiffuseColor = DirectX::XMFLOAT4(red, green, blue, alpha);
    return;
}


void LightClass::SetDirection(float x, float y, float z)
{
    mDirection = DirectX::XMFLOAT3(x, y, z);
    return;
}


DirectX::XMFLOAT4 LightClass::GetDiffuseColor()
{
    return mDiffuseColor;
}


DirectX::XMFLOAT3 LightClass::GetDirection()
{
    return mDirection;
}