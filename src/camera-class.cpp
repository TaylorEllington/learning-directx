#include "camera-class.h"

CameraClass::CameraClass()
{
    mPositionX = 0.0f;
    mPositionY = 0.0f;
    mPositionZ = 0.0f;

    mRotationX = 0.0f;
    mRotationY = 0.0f;
    mRotationZ = 0.0f;
}


CameraClass::CameraClass(const CameraClass& other)
{
}


CameraClass::~CameraClass()
{
}

void CameraClass::SetPosition(float x, float y, float z)
{
    mPositionX = x;
    mPositionY = y;
    mPositionZ = z;
    return;
}


void CameraClass::SetRotation(float x, float y, float z)
{
    mRotationX = x;
    mRotationY = y;
    mRotationZ = z;
    return;
}

DirectX::XMFLOAT3 CameraClass::GetPosition()
{
    return DirectX::XMFLOAT3(mPositionX, mPositionY, mPositionZ);
}


DirectX::XMFLOAT3 CameraClass::GetRotation()
{
    return DirectX::XMFLOAT3(mRotationX, mRotationY, mRotationZ);
}

void CameraClass::Render()
{
    DirectX::XMFLOAT3 up, position, lookAt;
    DirectX::XMVECTOR upVec, positionVec, lookAtVec;
    float yaw, pitch, roll;
    DirectX::XMMATRIX rotationMatrix;


    // Setup the vector that points upwards.
    up.x = 0.0f;
    up.y = 1.0f;
    up.z = 0.0f;

    // Setup the position of the camera in the world.
    position.x = mPositionX;
    position.y = mPositionY;
    position.z = mPositionZ;
    positionVec = DirectX::XMLoadFloat3(&position);

    // Setup where the camera is looking by default.
    lookAt.x = 0.0f;
    lookAt.y = 0.0f;
    lookAt.z = 1.0f;
    lookAtVec = DirectX::XMLoadFloat3(&lookAt);

    // Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
    pitch = mRotationX * 0.0174532925f;
    yaw   = mRotationY * 0.0174532925f;
    roll  = mRotationZ * 0.0174532925f;

    // Create the rotation matrix from the yaw, pitch, and roll values.
    rotationMatrix = DirectX::XMMatrixRotationRollPitchYaw(yaw, pitch, roll);
    

    // Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
    
    lookAtVec = DirectX::XMVector3TransformCoord(lookAtVec, rotationMatrix);



    upVec = DirectX::XMLoadFloat3(&up);
    upVec = DirectX::XMVector3TransformCoord(upVec, rotationMatrix);


    // Translate the rotated camera position to the location of the viewer
    lookAtVec = DirectX::XMVectorAdd(positionVec, lookAtVec);

    // Finally create the view matrix from the three updated vectors.
    mViewMatrix = DirectX::XMMatrixLookAtLH(positionVec, lookAtVec, upVec);

    return;
}

void CameraClass::GetViewMatrix(DirectX::XMMATRIX & viewMatrix)
{
    viewMatrix = mViewMatrix;
    return;
}

