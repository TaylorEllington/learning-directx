#pragma once

#include <d3d11.h>
//#include <d3dx11tex.h>

class TextureClass
{
public:
    TextureClass();
    TextureClass(const TextureClass&);
    ~TextureClass();

    bool Initialize(ID3D11Device*, WCHAR*);
    void Shutdown();
    ID3D11ShaderResourceView* GetTextureView();
    ID3D11Resource * getTexture();

private:
    ID3D11ShaderResourceView* mTextureView;
    ID3D11Resource* mTexture;
};
